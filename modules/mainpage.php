<?php
class mainpageClass{
    private $facebook;
    public function __construct(){
        $this -> facebook = Fcb::getInstance();
    }
    public function startAction(){
        $fb = Fcb::getInstance();
        if($fb ->is_authorized()){
            $smarty = UseSmarty::getInstance();
            $smarty ->assign('template', 'm_mainpage_startafterauth.tpl');
        }
        
    }
    public function nagrodyAction(){
        if (!$this->facebook->is_authorized()) {
            HTTP::Redirect(URLABS . 'join/');
        }
    }
    public function zasadyAction(){
        if (!$this->facebook->is_authorized()) {
 
            HTTP::Redirect(URLABS . 'join/');
        }
    }
    public function zwyciezcyAction(){
        if (!$this->facebook->is_authorized()) {
            HTTP::Redirect(URLABS . 'join/');
        }
    }
};