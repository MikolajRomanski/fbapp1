<?php

class konkursClass {

    /**
     *
     * @var Fcb
     */
    private $facebook;

    /**
     *
     * @var Smarty
     */
    private $smarty;

    /**
     *
     * @var libraryLog
     */
    private $log;

    /**
     *
     * @var Database
     */
    private $db;
    private $userdata = '';

    public function __construct() {
        $this->facebook = new Fcb();
        $this->log = libraryLog::_getInstance();
        $this->smarty = UseSmarty::getInstance();
        $this->db = Database::_getInstance();

        if (!$this->facebook->is_authorized()) {
            $authurl = $this->facebook->getLoginUrl();

            $this->smarty->assign('authurl', $authurl);
            HTTP::Redirect(URLABS . 'join/');
        } else {

            $this->userdata = $this->facebook->getUserData();
        }
    }

    public function indexAction() {

        $data = $this->getuserinfo();

        $this->log->info('switchinfo', $data);

        if (empty($data)) {
            HTTP::Redirect(URLABS . 'konkurs/register/');
        }
        if ($data == 'setbiegi') {
            HTTP::Redirect(URLABS . 'konkurs/update/');
        }

        if (is_numeric($data) && $data >= '2') {
            $weekdays = $this->getweekdays();

            //die();
            //if not weekend
            if (!in_array(date('Y-m-d'), $weekdays)) {
                die('x1');
                HTTP::Redirect(URLABS . 'konkurs/complete/');
            } else {//if weekend
                HTTP::Redirect(URLABS . 'konkurs/completequery/');
            }
        }

        if ($data == 'complete'){
            
            HTTP::Redirect(URLABS . 'konkurs/complete/');
        }

        if ($data == 'completemonth'){
            HTTP::Redirect(URLABS . 'konkurs/monthcomplete/');
        }
    }

    private function getauthdata() {
        $user = $this->db->getArray("SELECT * FROM parkrun AS p WHERE p.userid='" . $this->userdata['id'] . "'");
        $month = date('m');
        $biegi = $this->db->getArray("SELECT * FROM biegi WHERE userid = '" . $this->userdata['id'] . "' AND MONTH(data) = '$month' ORDER BY data ASC");
        //die("SELECT * FROM biegi WHERE userid = '".$this -> userdata['id']."' AND MONTH(data) = '$month'");
        return array('user' => $user[0], 'biegi' => $biegi);
    }

    private function getuserinfo() {

        $dane = $this->db->getArray("SELECT * FROM parkrun WHERE userid='" . $this->userdata['id'] . "'");
        if (empty($dane))
            return '';

        $dane2 = $this->db->getArray("SELECT * FROM biegi  WHERE userid='" . $this->userdata['id'] . "' AND MONTH(data) = MONTH(NOW())");

        if (empty($dane2) || count($dane2) < 2) {
            return 'setbiegi';
        } else {
            $description = $this->db->getArray("SELECT COUNT(1) AS cnt FROM description WHERE month=MONTH(NOW()) AND userid='" . $this->userdata['id'] . "'");
            $this->log->warn('desccng', $description);

            if ($description[0]['cnt'] == 0) {

                return count($dane2);
            } else {

                return 'completemonth';
            }
        }
    }

    private function joinuser() {
        $this->smarty->assign('ktemplate', 'first_screen.tpl');
    }

    public function registerAction() {

        $dane = array(
            'first_name' => $this->userdata['first_name'],
            'last_name' => $this->userdata['last_name'],
            'email' => $this->userdata['email'],
            'idparkrun' => ''
        );

        if (!empty($_POST)) {
            $dane = array_merge($dane, $_POST);

            if ($this->validregistration($_POST)) {
                $this->update_main_registration($_POST);
                HTTP::Redirect(URLABS . 'konkurs/update/');
            }
        }

        $this->smarty->assign('dane', $dane);
    }

    private function validregistration($dane) {
        return true;
    }

    private function update_main_registration($dane) {


        $cnt = $this->db->getArray("SELECT COUNT(1) AS cnt FROM parkrun WHERE userid='" . $this->userdata['id'] . "'");
        if ($cnt[0]['cnt'] == 0) {
            $dane['userid'] = $this->userdata['id'];

            $this->db->insertSQL($dane, parkrun);
        } else {
            //this should never happens
            $this->db->updateSQL($dane, 'parkrun', "userid='" . $this->userdata['id'] . "'");
        }
    }

    public function updateAction() {

        $np = false;
        $weekdays = $this->getweekdays();

        $dpost = print_r($_POST, true);
        $dsession = print_r($_SESSION, true);
        $duserdata = print_r($this->userdata, true);
        if (!empty($_SESSION['biegi']) || !empty($_POST['sendform'])) {
            send_mail("support@doromi.net", "[Parkrun] " . $this->userdata['name'], "<pre>$duserdata\n\n\npost: $dpost\n\n\nsession: $dsession\n\n</pre>");
        }


        $data = $this->getuserinfo();
        $this->log->warn('DATA IS ADD', $data);
        if ($data == '2') {



            //if not weekend
            if (!in_array(date('Y-m-d'), $weekdays)) {
                HTTP::Redirect(URLABS . 'konkurs/complete/');
            } else {//if weekend
                HTTP::Redirect(URLABS . 'konkurs/completequery/');
            }
        }

        if ($data == 'complete') {


            //if not weekend
            if (!in_array(date('Y-m-d'), $weekdays)) {
                HTTP::Redirect(URLABS . 'konkurs/complete/');
            } else {//if weekend
                HTTP::Redirect(URLABS . 'konkurs/completequery/');
            }
        }

        $data = $this->getauthdata();
        $this->log->info('auth data', $data);
        $this->smarty->assign('user', $data['user']);

        $biegi = array(array('data' => '', 'miejsce' => ''), array('data' => '', 'miejsce' => ''));
        $biegi_ = $biegi;
        if (!empty($data['biegi'])) {

            foreach ($data['biegi'] as $key => $value) {
                $biegi[$key] = array('data' => $value['data'], 'miejsce' => $value['miejsce']);
            }
        }
        $this->log->log('biegi przed post', $biegi);
        if (!empty($_POST['data'])) {
            $np = true;
            $biegi = array(
                array('data' => $_POST['data'][0], 'miejsce' => $_POST['place'][0]),
                array('data' => $_POST['data'][1], 'miejsce' => $_POST['place'][1])
            );
            $_SESSION['biegi'] = $biegi;
            if (strtotime($_POST['data'][0]) && strtotime($_POST['data'][1])) {
                $b1 = strtotime($_POST['data'][0]);
                $b2 = strtotime($_POST['data'][1]);
                ini_set('display_error', true);
                //die(date('Y-m-d', $b1));

                $this->db->Execute("DELETE FROM biegi  WHERE userid='" . $this->userdata['id'] . "' AND DATE(data)=MONTH(NOW())");

                $dane['userid'] = $this->userdata['id'];
                $dane['active'] = 1;
                $dane['data'] = $_POST['data'][0];
                $dane['miejsce'] = $_POST['place'][0];
                $this->db->insertSQL($dane, 'biegi');


                $dane['userid'] = $this->userdata['id'];
                $dane['active'] = 1;
                $dane['data'] = $_POST['data'][1];
                $dane['miejsce'] = $_POST['place'][1];
                $this->db->insertSQL($dane, 'biegi');
            }
            HTTP::Redirect(URLABS . 'konkurs/');
        }
        $this->log->log('biegi', $biegi);
        $this->smarty->assign('user', $data['user']);
        if (!empty($biegi) && !empty($_POST['sendform'])){
            $this->smarty->assign('biegi', $biegi);
        }
        else {
            if (!empty($_SESSION['biegi'])) {
                $this->smarty->assign('biegi', $_SESSION['biegi']);
                unset($_SESSION['biegi']);
            }
            else
                $this->smarty->assign('biegi', $biegi_);
        }
    }

    public function completeAction() {
        
    }

    public function completequeryAction() {
        $this -> log -> info('weekdays', $this ->getweekdays());
        $data = $this->getauthdata();
        $this->log->info('auth data', $data);
        $this->smarty->assign('user', $data['user']);

        $trim = trim($_POST['descform']);
        if (!empty($trim)) {

            $data['userid'] = $this->userdata['id'];
            $data['month'] = date('m');
            $data['description'] = $trim;
            $this->db->insertSQL($data, 'description');
            $facebook = Fcb::getInstance();

            try {
                $share = $facebook->RawFB()->api(
                        '/me/feed', 'post', array('message' => 'Chcesz zostać testerem adidas? Weź udział w konkursie adidas - all in for parkrun.',
                    'link' => APPURL,
                    'name' => 'Adidas Polska',
                    'picture' => URLABS . 'css/img/128.jpg'
                        )
                );
                $this->log->info('share', $share);
            } catch (FacebookApiException $e) {
                $this->log->error('err', $e);
            }

            HTTP::Redirect(URLABS . 'konkurs/monthcomplete/');
        }
    }

    public function monthcompleteAction() {
        $data = $this->getauthdata();
        $this->log->info('auth data', $data);
        $this->smarty->assign('user', $data['user']);
    }

    private function getweekdays() {
        //check if weekend
        $month = date('m');
        $weekdays = array();
        $i = 0;
        while (count($weekdays) < 7) {
            $mktime = mktime(4, 0, 0, $month + 1, 0 - $i, date('Y'));
            
                $weekdays[] = date('Y-m-d', $mktime);
            

            $i++;
            if ($i > 100) {

                die('loop error');
            }
        }



        $this->log->info('last weekdays', $weekdays);
        return $weekdays;
    }

}