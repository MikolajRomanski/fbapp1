<!doctype html>
<html xmlns='http://www.w3.org/1999/xhtml' lang='pl' xml:lang='pl' xmlns:fb='http://ogp.me/ns/fb#'>
<head>
	<title>Facebook Aplication</title>
        <base href="{$abs}" id="base_url" />
        <meta property="og:image" content="{$abs}css/img/128.jpg" />
        <meta property="og:appId" content="{$appId}" />
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<link rel="stylesheet" href="css/style.css?v=3.5" type="text/css" media="all" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src='js/jquery.cycle.all.js' type='text/javascript'></script>
	<script src='js/theme.js?v=0.3' type='text/javascript'></script>
        <script src='js/mik.js?2' type='text/javascript'></script>
        {literal}
	<script type='text/javascript'>/* <![CDATA[ */			
		//Initialize FB App
		window.fbAsyncInit = function() {
			FB.init({{/literal}
				appId      : '{$appId}', // App ID
				channelUrl : '{$abs}channel.html', // Channel File
				status     : true, // check login status
				cookie     : true, // enable cookies to allow the server to access the session
				xfbml      : true  // parse XFBML
			{literal}});
		    // Additional initialization code here
		    //FB.Canvas.setSize({ width: 810, height: 1320 });
		    //FB.Canvas.setSize();
		    FB.Canvas.setAutoGrow();
                    
		};
	
	  	// Load the SDK Asynchronously
		(function(d, debug){
			var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = "//connect.facebook.net/pl_PL/all" + (debug ? "/debug" : "") + ".js";
			ref.parentNode.insertBefore(js, ref);
		}(document, /*debug*/ false));
		
		function invite(title,msg) {
			FB.ui({ 
	   			method: 'apprequests', 
	   			message: msg,
	            data: 'Something to track',
	            title: title
	      	});
		}	
		
		function publishToFeed(_publishObj) {
			FB.ui({
				method: 'stream.publish',
				message: _publishObj.message,
				auto_publish:true,
				attachment: {
					media: [{ 
						type: "image",
                        href: _publishObj.appHref,
                        src: _publishObj.imgSrc
					}],
					name: _publishObj.name,
					caption: _publishObj.caption,
					description: ( _publishObj.description ),
      				href: _publishObj.appHref
				},
				action_links: [{ 
					text: _publishObj.text, 
					href: _publishObj.appHref 
				}]
				
			}, function(response) {
				if (response && response.post_id) { 
					// alert('Post was published.');
    			} else { 
    				// alert('Post was not published.');
				}	
			});
		}	
	
	// ******************* GA CODE *********************	
		/*
		 var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-00000000-0']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    //var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    //var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
	*/
	// *************************************************

	/* ]]> */</script>
        {/literal}
</head>

{include file=$template}
</html>
