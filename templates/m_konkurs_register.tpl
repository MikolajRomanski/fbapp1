<body id="cont01">
<div id="wrap">

{include file="i_header.tpl" nav='konkurs'}

<div id="content">
	<h2>rejestracja w konkursie</h2>
	<form id="form1" action="" method="post" accept-charset="utf-8">
		<div id="fieldsholder">
			<p id="idbox">
				<label for="startingID">ID parkrun</label>
				<input type="text" id="startingID" value='{$dane.idparkrun|stripslashes|escape}' name="idparkrun"/>
			</p>
			<p id="namebox">
				<label for="yourName">imię</label>
				<input type="text" id="yourName" value='{$dane.first_name|stripslashes|escape}' name="first_name"/>
			</p>
			<p id="surnamebox">
				<label for="yourSurname">nazwisko</label>
				<input type="text" id="yourSurname" value='{$dane.last_name|stripslashes|escape}' name="last_name"/>
			</p>
			<p id="emailbox">
				<label for="email">adres email</label>
				<input type="text" id="email" value='{$dane.email|stripslashes|escape}' email="email"/>
			</p>
			<input type="checkbox" id="acceptregulations" name="acceptregulations" value="accepted" />
		</div>
		<a id="setacceptregulations">Akceptuję regulamin i wyrażam zgodę na przetwarzanie moich danych osobowych w celach konkursowych, zgodnie z wymogami ustawy z dnia 29 sierpnia 1997r. o ochronie danych osobowych.</a>
		<p class="submitholder"><input type="submit" value="dalej"/></p>
	</form>
</div>

<div id="footer">
	<a href="regulamin_parkrun.pdf" id="regulation">regulamin</a>
	<a href="http://www.facebook.com/adidasRunningPL" target="blank" id="arp">www.facebook.com/adidasRunningPL</a>
</div>

</div>
                        {include file="i_footer.tpl"}
</body>