<body id="hp1">
<div id="wrap">

{include file="i_header.tpl" nav='main'}

<div id="content">
	<h2>CZYM JEST PARKRUN?</h2>
        <div class="parkrunlogo" style='margin-top: 40px;'><a href="http://parkrun.pl/" target="_blank"><span>oficjalna strona parkrun</span></a></div>
	<p id="first" style='margin-top: 70px;'>Parkrun to biegi na dystansie 5 km z pomiarem czasu. Organizowane w każdą sobotę o 9.00 rano w wybranych parkach Polski.</p>
	<p id="getstarted"><a href="/konkurs/">weź udział w konkursie</p>
	<p id="second">Więcej informacji o parkrun na oficjalnej stronie<a href="#">TUTAJ.</a></p>	
</div>

<div id="footer" style='margin-top: 140px;'>
	<a href="regulamin_parkrun.pdf" id="regulation">regulamin</a>
	<a href="http://www.facebook.com/adidasRunningPL" target="blank" id="arp">www.facebook.com/adidasRunningPL</a>
</div>

</div>
{include file="i_footer.tpl"}
</body>