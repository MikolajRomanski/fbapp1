<body id="cont06">
    <div id="wrap">

        {include file="i_header.tpl" nav='konkurs'}

        <div id="content">
            <h2>pytanie konkursowe</h2>
            <h3>Wakacje i bieganie - jak to połączysz?</h3>
            <form id="form6" action="" method="post" accept-charset="utf-8">
                <textarea rows="6" name="descform"></textarea>
            
            <p class="parkid"><img src="css/img/npark.png" alt="Numer Użytkownika Parkrun"/><span>{$user.idparkrun|stripslashes|escape}</span></p>
            <p class="submitholder"><input type="submit" value="dalej"/></p>
            </form>
        </div>

        <div id="footer">
            <a href="regulamin_parkrun.pdf" id="regulation">regulamin</a>
            <a href="http://www.facebook.com/adidasRunningPL" target="_blank" id="arp">www.facebook.com/adidasRunningPL</a>
        </div>

    </div>
            {include file="i_footer.tpl"}
</body>