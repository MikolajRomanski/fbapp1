<body id="hp">
    <div id="wrap">

        <div id="header">
            <p>Ukończ minimum 2 biegi parkrun w miesiącu, dołącz do naszej zabawy i weź udział w testach najnowszego sprzętu biegowego adidas!</p>
            <a href="/join/" class="start">weź udział</a>
        </div>

        <div id="content">
            <h2>CZYM JEST PARKRUN?</h2>
            <div class="parkrunlogo"><a href="http://parkrun.pl/" target="_blank"><span>oficjalna strona parkrun</span></a></div>
            <p id="first">Parkrun to biegi na dystansie 5 km z pomiarem czasu. Organizowane w każdą sobotę o 9.00 rano w wybranych parkach Polski.</p>
            <p id="second">Więcej informacji o parkrun na oficjalnej stronie<a href="http://parkrun.pl/" target="_blank">TUTAJ.</a></p>	
        </div>

        <div id="footer" style='margin-top: 140px;'>
            <a href="regulamin_parkrun.pdf" id="regulation">regulamin</a>
            <a href="http://www.facebook.com/adidasRunningPL" target="_blank" id="arp">www.facebook.com/adidasRunningPL</a>
        </div>

    </div>
    <div style="clear: both; height: 1px;"></div>
    {include file="i_footer.tpl"}
</body>