<body id="winners">
<div id="wrap">

{include file="i_header.tpl" nav='zwyciezcy'}

<div id="content">
	<h2>Zwycięzcy</h2>
	<div id="fieldsholder">{*}
		<div class="slideholder">
			<div class="slide"><div class="inner">
				<h3 class="month">czerwiec</h3>
				<p class="w01"><span>ID 99999</span> Marian Tadeusz Nowak z Grodziska Mazowieckiego</p>
				<p class="w02"><span>ID 99999</span> Marian Tadeusz Nowak z Grodziska Mazowieckiego</p>
				<p class="w03"><span>ID 99999</span> Marian Tadeusz Nowak z Grodziska Mazowieckiego</p>				
			</div></div>
			<div class="slide"><div class="inner">
				<h3 class="month">lipiec</h3>
				<p class="w01"><span>ID 99999</span> Marian Tadeusz Nowak z Grodziska Mazowieckiego</p>
				<p class="w02"><span>ID 99999</span> Marian Tadeusz Nowak z Grodziska Mazowieckiego</p>
				<p class="w03"><span>ID 99999</span> Marian Tadeusz Nowak z Grodziska Mazowieckiego</p>				
			</div></div>
		</div>{*}
                <p style="color: #000000; display: block; position: relative;">Tutaj może znaleźć się Twoje imię i nazwisko...</p>
	</div>
	{*}<h3 class="thanks">gratulujemy!</h3>{*}
</div>

<div id="footer">
	<a href="regulamin_parkrun.pdf" id="regulation">regulamin</a>
	<a href="http://www.facebook.com/adidasRunningPL" target="_blank" id="arp">www.facebook.com/adidasRunningPL</a>
</div>

</div>
{include file="i_footer.tpl"}
</body>