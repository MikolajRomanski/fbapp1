<body id="rules">
<div id="wrap">

{include file="i_header.tpl" nav='zasady'}

<div id="content">
	<h2>Zasady</h2>
	<div id="fieldsholder">
		<h3 style="background-image: url('css/img/reg1.jpg');height: 370px; top: 0;">1.   Aby wziąć udział w zabawie, ukończ 2 dowolne biegi parkrun w miesiącu. 2.   Odpowiedz na pytanie konkursowe. 3.   Co miesiąc wybieramy zwycięzcę - autora najciekawszej odpowiedzi. Otrzyma on sprzęt biegowy adidas. Dodatkowo - dwie wyróżnione osoby, dostaną od adidas upominiki. 4.   Zwycięzca, w ciągu miesiąca - tesuje swoją nagrodę. 5.   Relacje z testu pojawią się na stronie www.adidas.bieganie.pl oraz w newsletterze parkrun. <a href="#"></a></h3>
	</div>
</div>

<div id="footer">
	<a href="regulamin_parkrun.pdf" id="regulation">regulamin</a>
	<a href="http://www.facebook.com/adidasRunningPL" target="_blank" id="arp">www.facebook.com/adidasRunningPL</a>
</div>

</div>
{include file="i_footer.tpl"}
</body>