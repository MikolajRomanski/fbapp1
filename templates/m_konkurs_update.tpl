<body id="cont02">
<div id="wrap">

{include file="i_header.tpl" nav='konkurs'}

<div id="content">
	<h2>rejestracja w konkursie</h2>
	<form id="form2" action="" method="post" accept-charset="utf-8">
		<div id="fieldsholder">
			<h3 style="background-image: url('css/img/enter_1.png'); background-position: 0 4px; height: 38px;">Uzupełnij informacje o biegach, które ukończyłeś.</h3>
			
			<p id="start1box">
				<label for="start2">1. bieg</label>
				<input name="data[0]" type="text" id="start1date" value='{$biegi[0].data|stripslashes|escape}' placeholder='data biegu' title="Format: {$smarty.now|date_format:"%Y-%m-%d"}"/>
				<input name="place[0]" type="text" id="start1place" value='{$biegi[0].miejsce|stripslashes|escape}' placeholder='miejsce biegu' />
			</p>
			<p id="start2box">
				<label for="start2">2. bieg</label>
				<input name="data[1]" type="text" id="start2date" value='{$biegi[1].data|stripslashes|escape}' placeholder='data biegu'  title="Format: {$smarty.now|date_format:"%Y-%m-%d"}"/>
				<input name="place[1]" type="text" id="start2place" value='{$biegi[1].miejsce|stripslashes|escape}' placeholder='miejsce biegu' />
			</p>	
		</div>
		<p class="parkid"><img src="css/img/npark.png" alt="Numer Użytkownika Parkrun"/><span>{$user.idparkrun|stripslashes|escape}</span></p>
		<p class="submitholder"><input type="submit" value="dalej"/></p>
                <input type='hidden' name='sendform' value='1'/>
	</form>
</div>

<div id="footer">
	<a href="regulamin_parkrun.pdf" id="regulation">regulamin</a>
	<a href="http://www.facebook.com/adidasRunningPL" target="blank" id="arp">www.facebook.com/adidasRunningPL</a>
</div>

</div>
                {include file="i_footer.tpl"}
</body>