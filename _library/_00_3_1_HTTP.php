<?php
class HTTP {
    public static function Redirect($p_url = SITE_URI) {
        header("Location: ".$p_url);
        exit;
    }
    
    public static function Show($objectclass, $objectmethod){
        
        if(isset($_COOKIE['noredirect']) && $_COOKIE['noredirect']=='1') return false;
        
        //die('redirect while not in facebook iframe');
        $facebook = Fcb::getInstance();
        $uid = $facebook ->getUserData();
        $facebook -> logdata(empty($uid) ? '-' : $uid[0], 'redirect not pagetab!');
        $_SESSION['pagetab_redirect']['class'] = $objectclass;
        $_SESSION['pagetab_redirect']['method'] = $objectmethod;
        //die('redirect!');
        //HTTP::Redirect(FANPAGE_APP);
        return true;
    }
    
}
?>