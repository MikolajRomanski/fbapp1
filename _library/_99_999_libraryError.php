<?php

$dumperrors = array();
# Function for errorHandling

function shutdown($errno, $errstr, $file, $line, $context) {
    chdir(WORKING_DIR);
    libraryLog::_getInstance()->group_start($errstr, '#FF0000', true);
    libraryLog::_getInstance()->warn('$errstr', $errstr);
    libraryLog::_getInstance()->warn('$file', $file);
    libraryLog::_getInstance()->warn('$line', $line);
    libraryLog::_getInstance()->group_end();
    chdir($_SERVER['DOCUMENT_ROOT']);
    if ($error = error_get_last()) {

        libraryLog::_getInstance()->error($error['type'], $error['message'], true);
        libraryLog::_getInstance()->warn('Plik', $error['file']);
        libraryLog::_getInstance()->warn('Linia', $error['line']);



        $errfile = fopen(ERRORDIR . date("Y-m-d") . ".txt", "a");
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'no_referer';
        $url = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'no_host';
        $url .= isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : 'no_request_uri';
        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'no_ip';
        fputs($errfile, $ip . ' ' . date('H:i:s') . ':[' . $url . ' / ' . $referer . '] | ' . $error['message'] . ' - linia: ' . $error['line'] . "\n");
        $post = print_r($_POST, true);
        fputs($errfile, $post . "--\n");
        fclose($errfile);


        $server = $_SERVER;
        if (empty($server['HTTP_REFERER']))
            $server['HTTP_REFERER'] = '';
        
        $usmart = UseSmarty::getInstance();
        $usmart->assign("server", $server);
        $usmart->assign("get", print_r($_GET, true));
        $usmart->assign("post", print_r($_POST, true));
        $usmart->assign('time', date("Y-m-d H:i:s"));
        $usmart->assign('error', $error);
        
        global $developers_mails;
        $usmart->assign('developers_mails', $developers_mails);

        $error_debug = $usmart->fetch('Error/error_mail.tpl');
        if (!empty($developers_mails)) {
            foreach ($developers_mails as $value1) {
                send_mail($value1, "Błąd krytyczny " . SITE_URI, $error_debug, '', 1);
            }
        }


        if (isset($error['type']) && ($error['type'] == E_ERROR || $error['type'] == E_PARSE || $error['type'] == E_COMPILE_ERROR)) {
            //ob_clean();

            libraryLog::_getInstance()->warn('ob problem', ob_get_status());
            if (!PRODUCTION_SERVICE) {
                echo 'Not in production PRODUCTION_SERVICE<br><pre>';
                print_r($error);
                die('</pre>');
            }
            die();
        }
    }

    //by mail
}

set_error_handler("shutdown");
?>