<?php

class objJS {

    private $files = array();
    private $jscontent = array();

    public function __construct() {
        //require_once '3rdparty/jsmin.php';
        chdir(WORKING_DIR); 
    }

    public static function getInstance($_inst = null) {
        static $_inst = null;
        if ($_inst == null)
            $_inst = new objJS();
        return $_inst;
    }
    
    
    
    
    

    public function add($file, $path = '') {
        if($path == '') $path = 'js';
        $benchmark = Benchmark::_getInstance();
        $js_ = $benchmark->Start('Add JS ' . $file);
        //$js = objJS::getInstance();
        $this->addJsFile($file, $path);
        $benchmark->End($js_);
    }

    public function generate() {
        chdir(WORKING_DIR);
        $nj = array();
        if (JS_COMPRESS) {
            foreach ($this->files as $key => $value) {
                $filetime = filemtime($value);
                $filename = JSDIR . $key . '_' . $filetime . '_single_compress.js';
                if (!file_exists($filename)) {
                    $content[$key] = JSMin::minify(file_get_contents($value));
                    $plik = fopen($filename, "w");
                    fputs($plik, $content[$key]);
                    fclose($plik);
                }
                $nj[$key] = $filename;
            }
            $this->files = $nj;
        } else {
            foreach ($this->files as $key => $value) {
                
                $filetime = filemtime($value);
                $filename = JSDIR . $key . '_' . $filetime . '_single_normal.js';
                if (!file_exists($filename)) {
                    $content[$key] = file_get_contents($value);
                    $plik = fopen($filename, "w");
                    fputs($plik, $content[$key]);
                    fclose($plik);
                }
                $nj[$key] = $filename;
            }
            $this->files = $nj;
        }
        /*
        if (JS_ONE_FILE) {
            if (isset($nj))
                $nj = array();
            foreach ($this->files as $key => $value) {
                $content[$key] = file_get_contents($value);
            }
            $content = implode("\n//new file\n", $content);
            $sha1 = JSDIR . sha1($content) . '_onefile_.js';
            if (!file_exists($sha1)) {
                $plik = fopen($sha1, "w");
                fputs($plik, $content);
                fclose($plik);
            }
            else
                $content = $sha1;
            unset($nj);
            $nj[sha1($sha1)] = $content;
            $this->files = $nj;
        }*/

        if (JS_ONE_FILE) {
            if (isset($nj))
                $nj = array();
            foreach ($this->files as $key => $value) {
                $content[$key] = file_get_contents($value);
            }
            $content = implode("\n", $content);
            $sha1 = JSDIR . sha1($content) . '_onefile_.js';
            if (!file_exists($sha1)) {
                $plik = fopen($sha1, "w");
                fputs($plik, $content);
                fclose($plik);
            }
            else
                $content = $sha1;
            unset($nj);
            $nj[sha1($sha1)] = $sha1;
            $this->files = $nj;
        }

        
        return $this->files;
    }

    private function addJsFile($file, $path) {
        $filename = $path .'/' . Translate::make_filename($file);
        if (file_exists($filename)) {
            $this->files[Translate::make_url($filename) . sha1($filename)] = $filename;
        } else {
            libraryLog::_getInstance()->error('JS - nie istnieje', $filename);
        }
    }

}

?>