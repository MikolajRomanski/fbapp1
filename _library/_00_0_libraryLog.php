<?php

class libraryLog {

    /**
     *
     * @var libraryLog
     */
    private static $log = null;
    /**
     *
     * @var FirePHP
     */
    private static $fire = null;
    private $debug = true;
    
    
    public function __construct($head = false) {
        chdir(WORKING_DIR); 
        $this -> debug = empty($_COOKIE['firemik']) ? DEBUG : true;
        if ($this -> debug) {
            self::$fire = new FirePHP();
            if ($head || !empty($_COOKIE['firemik']))
                self::print_header();
        }
    }

    private function print_header() {
        self::$fire->group('Informacje podstawowe', array('Collapsed' => true,
            'Color' => '#598F65'));
        self::$fire->log('Wsparcie techniczne: Mikołaj Romański, mikolaj@doromi.net');
        self::$fire->log(strftime("%A, %d %B %Y, godzina: %H:%M:%S"), 'Czas');
        if (isset($_SERVER['HTTP_USER_AGENT']))
            self::$fire->log($_SERVER['REMOTE_ADDR'] . ' (' . $_SERVER['HTTP_USER_AGENT'] . ')', 'IP');
        self::$fire->info($_GET, 'GET');
        self::$fire->info($_POST, 'POST');

        if (function_exists('ini_get')) {
            $size = ini_get('upload_max_filesize');
            if(is_numeric($size)) $size = Files::convertBytes($size);
            self::$fire->info($size,'Upload max filesize');
        } else {
            self::$fire->warn('Max upload size not avaiable');
        }

        self::$fire->groupEnd();
    }

    /**
     *
     * @return libraryLog
     */
    public static function _getInstance($head = false) {
        if (self::$log == null) {
            self::$log = new libraryLog();
            if ($head)
                self::$log->print_header();
        }
        return self::$log;
    }

    /**
     * Wrzuć do loga
     * @@author Mikołaj Romański <mikolaj@doromi.net>
     * @param string $label
     * @param object $value
     */
    public function log($label = '', $value = '') {
        if (!$this->debug)
            return false;
        if (empty($value)) {
            $value = $label;
            $label = '';
        }
        self::$fire->log($value, $label);
    }

    /**
     * Wrzuć do loga
     * @@author Mikołaj Romański <mikolaj@doromi.net>
     * @param string $label
     * @param object $value
     */
    public function info($label = '', $value = '') {
        if (!$this->debug)
            return false;
        if (empty($value)) {
            $value = $label;
            $label = '';
        }
        self::$fire->info($value, $label);
    }

    /**
     * Wrzuć do loga
     * @@author Mikołaj Romański <mikolaj@doromi.net>
     * @param string $label
     * @param object $value
     */
    public function warn($label = '', $value = '') {
        if (!$this->debug)
            return false;
        if (empty($value)) {
            $value = $label;
            $label = '';
        }
        self::$fire->warn($value, $label);
    }

    /**
     * Wrzuć do loga
     * @@author Mikołaj Romański <mikolaj@doromi.net>
     * @param string $label
     * @param object $value
     */
    public function error($label = '', $value = '', $debug = false) {
        if (!$this->debug)
            return false;
        if (empty($value)) {
            $value = $label;
            $label = '';
        }
        self::$fire->error($value, $label);
        if ($debug)
            self::$fire->trace('Ścieżka błędu');
    }

    /**
     * Wyrzuć ścieżkę trace
     * @@author Mikołaj Romański <mikolaj@doromi.net>
     * @param <type> $name
     * @param <type> $array
     */
    public function trace($name) {
        if (!$this->debug)
            return false;
        if (empty($value)) {
            $value = 'no value!';
            $label = '';
        }
        self::$fire->trace($name);
    }

    /**
     * Wrzuć do loga array
     * @@author Mikołaj Romański <mikolaj@doromi.net>
     * @param <type> $name
     * @param <type> $array
     */
    public function table($name, $array) {
        if (!$this->debug)
            return false;
        self::$fire->table($name, $array);
    }

    public function group_start($name, $color, $collapsed = false) {
        if (!$this->debug)
            return false;
        self::$fire->group($name, array('Collapsed' => $collapsed,
            'Color' => $color));
    }

    public function group_end() {
        if (!$this->debug)
            return false;
        self::$fire->groupEnd();
    }

    public function _disable_debug($b = true) {
        if ($b)
            $this->debug = false;
        else
            $this->debug = true;
    }
    
    
    public function system_log($service_id, $description, $serialize = array(),  $client_id='', $department_id='', $group_id=''){
        
        $auth = null;
        $db = null;
        $auth = Authorization::_getInstance();
        $db = Database::_getInstance();
        $dane['created'] = time();
        $dane['admin_id'] = $auth ->getUserID();
        $dane['rocznik_id'] = $_SESSION['rocznik_id'];
        $dane['service_id'] = $service_id;
        $dane['description'] = $description;
        $dane['serialize'] = serialize($serialize);
        $dane['client_id'] = $client_id;
        $dane['department_id'] = $department_id;
        $dane['group_id'] = $group_id;
        
        $db ->insertSQL($dane,'_system_log');
    }

}

?>