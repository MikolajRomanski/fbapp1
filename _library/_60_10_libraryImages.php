<?php
class libraryImages {
    private $width = 200;
    private $height = '';
    private $rodzaj = 's';
    private $id = '';
    private $src = '';
    private $moduleId = false;
    private $realWidth = 0;
    private $realHeight = 0;
    private $dstWidth;
    private $dstHeight;
    private $imageType = '';
    private $modulename;
    private $filename ='';
    private $dstfilename = '';

    private $image;

    public function  __construct( $file, $filename, $type = '' ) {
        if( is_numeric($file) ) {
            $this -> id = $file;
            
            $sql = SQL::getInstance();
            $pyt = "SELECT `filename`, `extension`, `module`, `reference_id` FROM `_cms_pictures` WHERE `_cms_pictures_id`='".$this -> id."'";
            $odp = $sql -> getArray($pyt);
            if(isset($odp[0]))
            {
                $this -> filename = $odp['0']['filename'];
                $this -> imageType = strtolower(Files::get_type_by_extension($odp[0]['extension']));
                $this -> modulename = $odp[0]['module'];
                
                
                $this -> src = RESOURCES.'gallery/'.strtolower(Translate::make_filename($this -> modulename)).'/'.$this -> filename.'-oryg'.$odp[0]['extension'];
                libraryLog::_getinstance() -> info('PATH', $this -> src);
                $this -> rodzaj = 'k';
                
            }
            else {
                return false;
            }
            list($this -> realWidth, $this -> realHeight) = getimagesize($this -> src);
            
        }
        else
        {
            $this -> src = $file;
            $this -> filename = $filename;
            $this -> imageType = strtolower(Files::get_extension_by_filename($filename));
            libraryLog::_getinstance() -> log('$this -> src', $this -> src);
            libraryLog::_getinstance() -> log('$this -> filename', $this -> filename);
        }
        
    }

    /**
     *Określ szerokość docelową obrazka
     * @param <type> $value
     */
    public function setWidth($value) {
        $this -> width = $value;
    }

    /**
     *Określ wysokość docelową obrazka
     * @param <type> $value
     */
    public function setHeight($value) {
        $this -> height = $value;
        
    }

    /**
     *Ustala rodzaj operacji na zdjęciu
     * @param <type> $value
     */
    public function setRodzaj($value) {
        $this -> rodzaj = (!empty($value)) ? $value : 's';
    }

    /**
     *Definiuje Id galerii
     * @param <type> $value
     */
    public function setModule($value) {
        $this -> galleryId = $value;
    }

    public function getWidth(){
        return $this -> realWidth;
    }
    public function getHeight(){
        return $this -> realHeight;
    }

    

    /**
     * Dodaje zdjęcie do odpowiedniej galerii
     */
    public function addPicture2Module( $modulename, $reference_id ) {
        $this -> modulename = $modulename;
        libraryLog::_getinstance() -> log('Image: ', $this -> src);
        
        if(!$this -> galleryId) return false;

        switch ($this -> rodzaj) {

            case 'k':
                $filename = $this -> kadr_image($this -> width, $this -> height );
                libraryLog::_getinstance() -> info('rodzaj', 'kadruje');
            break;
            case 'w':
                $filename = $this ->scale_image($this -> width, $this -> height );
                libraryLog::_getinstance() -> info('rodzaj', 'kadruje');
            break;
            default:
                $filename = $this -> scale_image($this -> width, $this -> height );
                libraryLog::_getinstance() -> info('rodzaj', 'skaluje');
            break;

        }


        return $filename;
    }

    /**
     *Skaluje obrazek proporcjonalnie do wymiarów. Skalowanie nie nie gwarantuje wymiarów obrazka $widthx$height
     * @param <type> $width
     * @param <type> $height
     */
    public function scale_image($width, $height, $save = true) {
        if($height == '') $height = 100000;
        if(!$this -> getImage())
        {
            
            libraryLog::_getinstance() -> error('IMAGE', 'getImage() error');
            return 4;
        }
        
        $img = $this -> wpasujImage($width, $height, $save);
        libraryLog::_getinstance() -> info('WpasujImage()', $img );
        return $img;
    }

    /**
     *Otwórz obrazek na potrzeby biblioteki i na zewnątrz
     * @return <type>
     */
    public function getImage() {
        if($this -> id != '') {
            libraryLog::_getinstance() -> info('Image Id', 'znany: '.$this -> id);
            $sql = SQL::getInstance();
            $pyt = "SELECT `filename`, `extension`, `module`, `reference_id` FROM `_cms_pictures` WHERE `_cms_pictures_id` = '".$this -> id."'";
            $odp = $sql -> getArray($pyt);
            libraryLog::_getinstance() -> info('IMAGE DATA', $pyt);
            if(!$odp) return false;
            
            
            $this -> filename = $odp[0]['filename'];
            $this -> moduleId = $odp[0]['reference_id'];
            $this -> imageType = strtolower($odp[0]['extension']);
            $this -> modulename = $odp[0]['module'];

            $this -> src = RESOURCES.'gallery/'.Translate::make_filename($odp[0]['module']).'/'.$odp[0]['filename'].'-oryg'.$odp[0]['extension'];
            libraryLog::_getinstance() -> info('PLIK', $this -> src);
            
        }
        else
        {
            libraryLog::_getinstance() -> info('Image Id', 'nieznany ');
        }

        list($this -> realWidth, $this -> realHeight) = getimagesize($this -> src);
        libraryLog::_getinstance() -> info($this -> src, $this -> realWidth.'x'.$this -> realHeight);
        
        libraryLog::_getinstance() -> warn('Typ obrazka', $this -> imageType);
        libraryLog::_getinstance() -> info('image_sizes', $this -> realWidth.'x'.$this -> realHeight);

        switch(strtolower($this -> imageType)) {
            case '.png':
                if(!$this -> image = imagecreatefrompng($this -> src)) libraryLog::_getinstance() -> error('PNG', 'nie otwarto pliku '.$this -> src);
                else libraryLog::_getinstance() -> log('PNG', 'Otwarto plik '.$this -> src);
            break;
            case '.jpg':
                if(!$this -> image = imagecreatefromjpeg($this -> src)) libraryLog::_getinstance() -> error('JPG', 'nie otwarto pliku '.$this -> src);
                else libraryLog::_getinstance() -> log('JPG', 'Otwarto plik '.$this -> src);
            break;
            case '.jpeg':
                if(!$this -> image = imagecreatefromjpeg($this -> src)) libraryLog::_getinstance() -> error('JPEG', 'nie otwarto pliku '.$this -> src);
                else libraryLog::_getinstance() -> log('JPEG', 'Otwarto plik '.$this -> src);
            break;
            case '.gif':
                if( !$this -> image = imagecreatefromgif($this -> src) ) libraryLog::_getinstance() -> error('GIF', 'nie otwarto pliku '.$this -> src);
                else libraryLog::_getinstance() -> log('GIF', 'Otwarto plik '.$this -> src);
            break;
        }
        return $this -> image;

    }


    private function wpasujImage($width, $height, $save = '') {
        libraryLog::_getinstance() -> log('IMG data', 'Oryg: '.$this -> realWidth.'x'.$this -> realHeight.' Cel: '.$width.'x'.$height);

        if( $width < $this -> realWidth || $height < $this -> realHeight) {

            libraryLog::_getinstance() -> log('Wpasuj', 'przeskalowuje '.$this -> rodzaj);

            $wskaznik1=$width/$this -> realWidth;
            $wskaznik2=$height/$this -> realHeight;

            if($this -> rodzaj == 's'){
                if($wskaznik1<=$wskaznik2) $wskaznik=$wskaznik1;
                else $wskaznik=$wskaznik2;
            }
            else{
                if($wskaznik1<=$wskaznik2) $wskaznik=$wskaznik2;
                else $wskaznik=$wskaznik1;
            }
            
            $nx=floor($this -> realWidth * $wskaznik);
            $ny=floor($this -> realHeight * $wskaznik);
            
            $this -> dstWidth = $nx;
            $this -> dstHeight = $ny;

            
            $nowy=imagecreatetruecolor($this -> dstWidth,$this -> dstHeight);
            imagealphablending($nowy, false);
            imagesavealpha($nowy, true);

            
            libraryLog::_getinstance() -> info('Realne docelowe wymiary ', $this -> dstWidth.'x'.$this -> dstHeight);

            
            $color = imagecolorallocate($nowy, 255, 255, 255);
            imagecopyresampled($nowy,$this -> image, 0, 0, 0, 0, $this -> dstWidth, $this -> dstHeight, $this -> realWidth, $this -> realHeight);
            
            
            
            imagedestroy($this -> image);
            $this -> image = $nowy;
            return $this -> image;
        }
        else {
            return $this -> image;
            $this -> dstWidth = $this -> realWidth;
            $this -> dstHeight = $this -> realHeight;
            
            $this -> dstfilename = Translate::make_url($this -> filename.'_'.$this -> width.'_'.$this -> height).$this -> imageType;
            
            $dst=RESOURCES.'gallery/'.strtolower(Translate::make_filename($this -> modulename)).'/'.$this -> dstfilename;

            $nowy=imagecreatetruecolor($this -> dstWidth,$this -> dstHeight);
            imagealphablending($nowy, false);
            imagesavealpha($nowy, true);
            $color = imagecolorallocate($nowy, 255, 255, 255);
            imagecopyresampled($nowy,$this -> image, 0, 0, 0, 0, $this -> dstWidth, $this -> dstHeight, $this -> realWidth, $this -> realHeight);
            if($save == true)
            {
                $this -> saveImage($nowy, $dst);
                imagedestroy($nowy);
            }
            else return $nowy;
            
            imagedestroy($this -> image);
            $this -> image = $nowy;
            
        }
        return $this -> image;

        
        
    }

    public function saveImage($image, $dst)
    {

        switch($this -> imageType)
            {
                case '.png':
                    if(!imagepng($image, $dst)) return false;
                break;
                case '.jpeg':
                case '.jpg':
                    if(!imagejpeg($image, $dst, 91)) return false;
                break;
                case '.gif':
                    if(!imagegif($image, $dst)) return false;
                break;
                default:
                    return false;
                break;
            }
        return true;
    }

    /**
     *Kadruje obrazek proporcjonalnie do wymiarów, wybierając jak największą powierzchnię zdjęcia aby zmieściła się w kadrze. Gwarantuje wymiary obrazka
     * @param <type> $width
     * @param <type> $height
     */
    public function kadr_image($width, $height, $save = true) {
        if($height=='') return 5;

        if(!$this -> getImage())
        {
            libraryLog::_getinstance() -> error('IMAGE', 'getImage() error');
            return 4;
        }

        
        $szer = $this -> realWidth;
        $wys = $this -> realHeight;
        
        $wx = $width;
        $wy = (is_array($height)) ? $height[0] : $height;

        //wyliczam wskaźniki
        $w_x = $szer/$wx;
        $w_y = $wys/$wy;
        $wskaznik_big = ($w_x<$w_y) ? $w_y : $w_x;
        $wskaznik_small = ($w_x>$w_y) ? $w_y : $w_x;
        $wx = $wx * $wskaznik_small;
        $wy = $wy * $wskaznik_small;
        libraryLog::_getinstance() -> info('Wskaźniki', "$wskaznik_big ($wx)/ $wskaznik_small ($wy)");


        $rx=floor(($szer-$wx)/2);
        $ry=floor(($wys-$wy)/2);
        
        $nowy=imagecreatetruecolor($wx,$wy);
        imagealphablending($nowy, false);
        imagesavealpha($nowy, true);
        $color = imagecolorallocate($nowy, 255, 255, 255);
        $this -> dstfilename = Translate::make_url($this -> filename.'_'.$this -> width.'_'.$this -> height).$this -> imageType;
        $dst=RESOURCES.'gallery/'.strtolower(Translate::make_filename($this -> modulename)).'/'.$this -> dstfilename;
        imagecopy($nowy,$this -> image,0,0,$rx,$ry,$wx,$wy);
        imagedestroy($this -> image);
        $oryg_realWidth = $this -> realWidth;
        $oryg_realHeight = $this -> realHeight;
        $this -> realWidth = $wx;
        $this -> realHeight = $wy;
        $this -> image = $nowy;
        $this -> image = $this -> dopasujImage($width, (is_array($height)) ? $height[0] : $height, false);
        $nowy = $this -> image;
        $this -> realWidth = $oryg_realWidth;
        $this -> realHeight = $oryg_realHeight;
        if($save == true)
        {
            $this -> saveImage($nowy, $dst);
            imagedestroy($nowy);
        }
        else return $nowy;
        
        return $this -> filename;
    }

    private function dopasujImage($width, $height, $save = true) {
        libraryLog::_getinstance() -> log('IMG data', 'Oryg: '.$this -> realWidth.'x'.$this -> realHeight.' Cel: '.$width.'x'.$height);

        if( $width < $this -> realWidth || $height < $this -> realHeight) {

            libraryLog::_getinstance() -> log('Dopasuj', 'wpasowuje w wymiary');

            $wskaznik1=$width/$this -> realWidth;
            $wskaznik2=$height/$this -> realHeight;

            if($wskaznik1<=$wskaznik2) $wskaznik=$wskaznik1;
            else $wskaznik=$wskaznik2;

            $nx=floor($this -> realWidth * $wskaznik);
            $ny=floor($this -> realHeight * $wskaznik);

            $this -> dstWidth = $nx;
            $this -> dstHeight = $ny;

            $nowy=imagecreatetruecolor($this -> dstWidth,$this -> dstHeight);
            imagealphablending($nowy, false);
            imagesavealpha($nowy, true);

            
            libraryLog::_getinstance() -> info('Realne docelowe wymiary ', $this -> dstWidth.'x'.$this -> dstHeight);


            $color = imagecolorallocate($nowy, 255, 255, 255);
            imagecopyresampled($nowy,$this -> image, 0, 0, 0, 0, $this -> dstWidth, $this -> dstHeight, $this -> realWidth, $this -> realHeight);
            if($save == true)
            {
                $this -> dstfilename = Translate::make_url($this -> filename.'_'.$this -> width.'_'.$this -> height).$this -> imageType;
                $dst=RESOURCES.'gallery/'.strtolower(Translate::make_filename($this -> modulename)).'/'.$this -> dstfilename;
                libraryLog::_getinstance() -> info('DST', $dst);
                imagejpeg($nowy,$dst,89);

                $this -> saveImage($nowy, $dst);
                imagedestroy($nowy);
            }
            else return $nowy;

            imagedestroy($this -> image);
            $this -> image = $nowy;
            return $this -> image;
        }
        else {

            $this -> dstWidth = $width;
            $this -> dstHeight = $height;

            $this -> dstfilename = Translate::make_url($this -> filename.'_'.$this -> width.'_'.$this -> height).$this -> imageType;

            $dst=RESOURCES.'gallery/'.strtolower(Translate::make_filename($this -> modulename)).'/'.$this -> dstfilename;

            $nowy=imagecreatetruecolor($this -> dstWidth,$this -> dstHeight);
            imagealphablending($nowy, false);
            imagesavealpha($nowy, true);
            $color = imagecolorallocate($nowy, 255, 255, 255);
            imagecopyresampled($nowy,$this -> image, 0, 0, 0, 0, $this -> dstWidth, $this -> dstHeight, $this -> realWidth, $this -> realHeight);
            if($save == true)
            {
                $this -> saveImage($nowy, $dst);
                imagedestroy($nowy);
            }
            else return $nowy;

            imagedestroy($this -> image);
            $this -> image = $nowy;

        }
        return $this -> image;



    }

    /**
     * Wygeneruj ikonę systemową
     */
    public function generate_system_element()
    {
        $this -> width = IMAGE_SYSTEM_X;
        $this -> height = IMAGE_SYSTEM_Y;
        $this -> rodzaj = IMAGE_SYSTEM_TYPE;
        
        

        $this -> getImage();
        libraryLog::_getinstance() -> info('$this -> image', $this -> image);
        libraryLog::_getinstance() -> info('SYSTEM', $this -> rodzaj);
        if($this -> rodzaj == 'k')
        {
            
            $plik = $this -> kadr_image($this -> width, $this -> height, false);
        }
        else
        {
            $plik = $this -> wpasujImage($this -> width, $this -> height, false);
        }

        if(!is_dir(TMPDIR.'system_items/'.Translate::make_filename($this -> modulename).'/'))
        {
            mkdir(TMPDIR.'system_items/'.Translate::make_filename($this -> modulename).'/', 0777);
            chmod(TMPDIR.'system_items/'.Translate::make_filename($this -> modulename).'/', 0777);
        }
        switch($this -> imageType) {
            case '.png':
                imagepng($plik, TMPDIR.'system_items/'.Translate::make_filename($this -> modulename).'/'.Translate::make_url($this -> filename).$this -> imageType);
            break;
            case '.jpg':
                imagejpeg($plik, TMPDIR.'system_items/'.Translate::make_filename($this -> modulename).'/'.Translate::make_url($this -> filename).$this -> imageType, 95);
            break;
            case '.jpeg':
                imagejpeg($plik, TMPDIR.'system_items/'.Translate::make_filename($this -> modulename).'/'.Translate::make_url($this -> filename).$this -> imageType, 95);
            break;
            case '.gif':
                imagegif($plik, TMPDIR.'system_items/'.Translate::make_filename($this -> modulename).'/'.Translate::make_url($this -> filename).$this -> imageType);
            break;
        }

        imagedestroy($plik);
    }

    public function create_avatar($uid){
        $image = $this -> getImage();

        $image = $this -> wpasujImage(IMAGE_AVATAR_X, IMAGE_AVATAR_Y, false);

        if(!is_dir(RESOURCES.'cmsavatars/')){
            mkdir(RESOURCES.'cmsavatars/');
            chmod(RESOURCES.'cmsavatars/', 0777);
            $plik=fopen(RESOURCES."cmsavatars/index.html","w");
            fputs($plik,"");
            fclose($plik);
        }
        $dst = RESOURCES.'cmsavatars/'.$uid.$this -> imageType;
        
        $this -> saveImage($image, $dst);
        
        copy($this -> src, RESOURCES.'cmsavatars/'.$uid.'_'.$this -> imageType );


        $dane['avatar'] = $this -> imageType;
        $sql = SQL::getInstance();
        $r = "SELECT * FROM `_users` WHERE `id`='$uid'";

        $r = $sql -> Execute($r);
        $updateSQL = $sql -> GetUpdateSQL($r, $dane);
        
        $sql -> Execute($updateSQL);
    }

    /**
     * @author Mikołaj Romański
     *Przekształca pojedyncze zdjęcie do odpowiednich wymiarów $sizes, zapisując je w $dst
     * @param array $sizes
     * @param string $dst
     */
    public function parse_for_size($sizes, $dst){
        libraryLog::_getinstance() -> info('Docelowe rozmiary obrazka', $sizes);
        libraryLog::_getinstance() -> info('Target pliku', $dst);
        $newwidth = $sizes['x'];
        $newheight = $sizes['y'];
        if(preg_match('/k/', $newheight)){
            libraryLog::_getinstance() -> info('Tryb przekształcenia obrazka', 'kadrowanie');
            $newheight_ = explode('k', $newheight);
            $newheight = $newheight_;
            if(empty($newheight)){
                libraryLog::_getinstance() -> error('Błąd wymiaru', 'Wymiar wysokości nie może być pusty przy kadrowaniu');
                return false;
            }
            $nowy = $this -> kadr_image($newwidth, $newheight, false);
            $this -> saveImage($nowy, $dst);
            imagedestroy($nowy);
            
        }
        else {
            libraryLog::_getinstance() -> info('Tryb przekształcenia obrazka', 'wpasowanie');
            libraryLog::_getinstance() -> warn('TODO LibraryImages#525', 'Dynamiczna regeneracja wpasowana');
        }
    }

    /**
     *@author Mikołaj Romański
     * Wykadruj fragment z oryginału oraz przeskaluj go do zadanej wielkości
     * @param <type> $x
     * @param <type> $y
     * @param <type> $width
     * @param <type> $height
     * @param <type> $dstwidth
     * @param <type> $dstheight
     */
    public function cut_image($x, $y, $width, $height, $dstwidth, $dstheight){
        $save = false;
        if($height=='') return 5;

        if(!$this -> getImage())
        {
            libraryLog::_getinstance() -> error('IMAGE', 'getImage() error');
            return 4;
        }


        $szer = $this -> realWidth;
        $wys = $this -> realHeight;

        $wx = $width;
        $wy = (is_array($height)) ? $height[0] : $height;



        $wx = $width;
        $wy = $height;

        $rx=$x;
        $ry=$y;

        $nowy=imagecreatetruecolor($wx,$wy);
        imagealphablending($nowy, false);
        imagesavealpha($nowy, true);
        $color = imagecolorallocate($nowy, 255, 255, 255);
        $this -> dstfilename = Translate::make_url($this -> filename.'_'.$this -> width.'_'.$this -> height).$this -> imageType;
        
        imagecopy($nowy,$this -> image,0,0,$rx,$ry,$wx,$wy);
        libraryLog::_getinstance() -> warn('imagecopyt', "0,0,$rx,$ry,$wx,$wy");
        //imagecopy($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h)
        imagedestroy($this -> image);
        
        $oryg_realWidth = $this -> realWidth;
        $oryg_realHeight = $this -> realHeight;
        $this -> realWidth = $wx;
        $this -> realHeight = $wy;
        $this -> image = $nowy;
        $this -> image = $this -> dopasujImage($dstwidth, $dstheight, false);
        $nowy = $this -> image;
        
        $this -> realWidth = $oryg_realWidth;
        $this -> realHeight = $oryg_realHeight;
        return $nowy;

    }

    
    /**
     *Get image type 
     */
    public function imageType(){
        $type = getimagesize($this -> src);
        libraryLog::_getInstance() -> info('imagetype', getimagesize($this -> src));
        switch($type['mime']){
            case 'image/jpg':
            case 'image/pjpg':
            case 'image/jpeg':
            case 'image/pjpeg':
                $this -> imageType = '.jpg';
                break;
            case 'image/png':
                $this -> imageType = '.png';
                break;
            case 'image/gif':
                $this -> imageType = '.gif';
                break;
        }
        return $this -> imageType;
    }
}
?>