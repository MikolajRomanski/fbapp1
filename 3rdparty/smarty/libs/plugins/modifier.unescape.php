<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * file modifier.unescape.php
 *
 * Smarty unescape modifier plugin
 *
 * Type:     modifier<br>
 * Name:     unescape<br>
 * Purpose:  Unescape the string according to escapement type (reverse function to smarty_modifier_escape)
 * @copyright Copyright (c) 2009, ICEshop BV
 * @param string
 * @param html|htmlall|url|urlpathinfo|quotes|javascript
 * @return string
 */
function smarty_modifier_unescape($string, $esc_type = 'html', $char_set = 'ISO-8859-1') {
  switch ($esc_type) {
    case 'html':
        return htmlspecialchars_decode($string, ENT_QUOTES);

    case 'htmlall':
        return html_entity_decode($string, ENT_QUOTES, $char_set);

    case 'url':
    case 'urlpathinfo':
        return rawurldecode($string);

    case 'quotes':
        return str_replace("\'", "'", $string);

    case 'javascript':
        return strtr($string, array('\\\\'=>'\\',"\\'"=>"'",'\\"'=>'"',"\\r"=>'\r',"\\n"=>'\n','<\/'=>'</'));

    case 'mail':
        return str_replace(array(' [AT] ', ' [DOT] '),array('@', '.'), $string);

    default:
        return $string;
  }
}

?>