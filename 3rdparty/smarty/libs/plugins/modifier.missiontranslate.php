<?php
function smarty_modifier_missiontranslate($string, $mission_id = null, $strreplace = '')
{
    $mission = missions::_getInstance($mission_id);
    $str = $mission -> getObject($mission_id) ->missionInformations($string);
    if($strreplace == '') return $str;
    
    $str = sprintf($str, "$strreplace");
    return $str;
    
}
?>