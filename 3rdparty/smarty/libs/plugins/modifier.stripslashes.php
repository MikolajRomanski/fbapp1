<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty fck modifier plugin
 *
 * Type:	 modifier<br>
 * Name:	 stripslashes<br>
 * Purpose:  stripslashes
 * @param string
 * @return string
 */

function smarty_modifier_stripslashes( $string )
{
	return stripslashes( $string );
}

?>