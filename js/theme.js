function checkForm(jobject) {
	var $ = jQuery,
		empty = false,
		inputs = jobject.find('input[type=text]'),
		checkboxes = jobject.find('input[type=checkbox]'),
		textareas = jobject.find('textarea');
		
	inputs.each(function(){
		if ( $(this).val() == '' ) {
            empty = true;
            return;
        };
	})
	checkboxes.each(function(){
		if ( !$(this).prop('checked') ) {
            empty = true;
            return;
        };
	})
	textareas.each(function(){
		if ( $(this).val() == '' ) {
            empty = true;
            return;
        };
	})
	//console.log(empty);
	if ( empty ) {
		$('.submitholder input').hide();
	} else {
		$('.submitholder input').show();		
	}
	//if ( empty ) {return false;} else {return true;};
}

jQuery(function($){
	$('.inputsholder input').on('focus', function() {
        $(this).removeClass('empty');
	}).on('blur', function() {
        if ($(this).val()=='') $(this).addClass('empty'); 
    });
    
    $('#setacceptregulations').click(function(){
		var checkBoxes = $('#acceptregulations'),
	   		switcher = $('#setacceptregulations');
	   	checkBoxes.prop("checked", !checkBoxes.prop("checked"));
	   	if ($('#acceptregulations').prop('checked')) {
		   	switcher.addClass('accepted');
		} else {
			switcher.removeClass('accepted');
		}
		checkForm($('#form1'));	
    });
    
    if ($('#form1').length > 0) {
    	$('.submitholder input').hide();
		$('#form1 input[type=text]').change(function(){
			checkForm($('#form1'));			
		}).keyup(function(){
			checkForm($('#form1'));
		})

    }
    if ($('#form2').length > 0) {
    	$('.submitholder input').hide();
    	$('#form2 input[type=text]').change(function(){
			checkForm($('#form2'));			
		}).keyup(function(){
                        checkForm($('#form1'));
                })
    }
    if ($('#form6').length > 0) {
    	$('.submitholder input').hide();
    	$('#form6 textarea').change(function(){
			checkForm($('#form6'));			
		}).keyup(function(){
                        checkForm($('#form1'));
                })
    }
    
    $('.slideholder').after('<a class="arrow" id="prev2"><a class="arrow" id="next2">').cycle({
	    fx: 'scrollHorz',
	    next:   '#next2', 
		prev:   '#prev2'
    }).cycle('pause');
})
