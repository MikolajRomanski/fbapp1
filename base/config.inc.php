<?php
require_once 'base/config_host.php';
require_once('base/config_var.php');
if(!is_dir(TMPDIR)) die('no TMPDIR - check configuration');
umask(0);
if(!is_dir(ERRORDIR)) {
    //die('no ERRORDIR - check configuration');
    mkdir(ERRORDIR,0777);
    touch(ERRORDIR."index.html");
}
if(!is_dir(TEMPLATESC)) {
    mkdir(TEMPLATESC, 0777);
    touch(TEMPLATESC."index.html");
   
}
if(!is_dir(TEMPLATESA)) {
    mkdir(TEMPLATESA, 0777);
    touch(TEMPLATESA."index.html");
}
if(!is_dir(JSDIR)) {
    mkdir(JSDIR, 0777);
    touch(JSDIR."index.html");
}
if(!is_dir(CSSDIR)) {
    mkdir(CSSDIR, 0777);
    touch(CSSDIR."index.html");
}
error_reporting(E_ALL);
ini_set('display_errors', false);
ini_set( "memory_limit" , "1024M" );
session_start();

if(isset($_SERVER['HTTPS'])) $s = $_SERVER['HTTPS'] != "on" ? '' : 's';
else $s = '';

?>